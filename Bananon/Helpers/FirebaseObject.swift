//
//  FirebaseObject.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation

protocol FirebaseObject {
    func toData() -> [String: Any];
}
