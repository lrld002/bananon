//
//  ObjectableDelegate.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 21/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation

protocol ObjectableDelegate {
    func itemWith(PID: String, didReceive objectableValue: Bool)
}
