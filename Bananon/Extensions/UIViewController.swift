//
//  UIViewController.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func hideKeyboardWhenTappedArround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector (UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print("UP")
            print("Current frame Y origin: \(self.view.frame.origin)")
            if self.view.frame.origin.y >= 0 {
                self.view.frame.origin.y -= keyboardSize.height
                self.view.setNeedsDisplay()
            }
        }
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFRame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFRame, to: view.window)
        
        if notification.name == UIApplication.keyboardWillHideNotification {
            view.frame.origin.y = 0 + UIApplication.shared.statusBarFrame.height
        } else {
            view.frame.origin.y -= keyboardViewEndFrame.height
        }
    }
    
}

