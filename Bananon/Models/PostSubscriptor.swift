//
//  PostSubscriptor.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation

class PostSubscriptor: FirebaseObject {
    
    var uid: String!
    var pushIds: [String]? = nil
    
    init(uid: String, pushIds: [String]) {
        self.uid = uid
        self.pushIds = pushIds
    }
    
    init(dictionary: [String: Any]?) {
        self.uid = dictionary?["uid"] as? String
        self.pushIds = (dictionary?["pushIds"] as? [String])
    }
    
    func toData() -> [String : Any] {
        let data = [
            "uid": uid,
            "pushIds": pushIds ?? []
            ] as [String: Any]
        return data
    }
    
}
