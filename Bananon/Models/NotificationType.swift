//
//  NotificationType.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation

enum NotificationType: Int {
    case Test = 0
    case Announcement = 1
    case DataTransfer = 2
    case UserInteraction = 3
}
