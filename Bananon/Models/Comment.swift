//
//  Comment.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation

class Comment: FirebaseObject {
    
    var uid: String!
    var cid: String!
    var pid: String!
    var timestamp: Int!
    var text: String!
    
    init(uid: String, cid: String, pid: String, timestamp: Int, text: String) {
        self.uid = uid
        self.cid = cid
        self.pid = pid
        self.timestamp = timestamp
        self.text = text
    }
    
    init(dictionary: [String: Any]?) {
        self.uid = dictionary?["uid"] as? String
        self.cid = dictionary?["cid"] as? String
        self.pid = dictionary?["pid"] as? String
        self.timestamp = dictionary?["timestamp"] as? Int
        self.text = dictionary?["text"] as? String
    }
    
    func toData() -> [String : Any] {
        let currentTimestamp =  NSDate().timeIntervalSince1970 * 1000
        let data = [
            "uid": uid as Any,
            "cid": cid as Any,
            "pid": pid as Any,
            "timestamp": currentTimestamp.rounded(),
            "text": text as Any,
            
            ] as [String: Any]
        
        return data
    }
    
    static func == (lhs: Comment, rhs: Comment) -> Bool {
        return lhs.uid == rhs.uid && lhs.pid == rhs.pid && lhs.cid == rhs.cid
    }
    
}

