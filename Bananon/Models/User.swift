//
//  User.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation

class User : FirebaseObject {
    
    var uid: String!
    var timestamp: Int64!
    
    init(){
        self.uid = ""
        self.timestamp = Int64(NSDate().timeIntervalSince1970 * 1000)
    }
    
    init(id: String, timestamp: Int64) {
        self.uid = id
        self.timestamp = timestamp
    }
    
    init(dictionary: [String: Any]?) {
        self.uid = dictionary?["uid"] as? String
        self.timestamp = dictionary?["timestamp"] as? Int64
    }
    
    func toData() -> [String : Any] {
        let data: [String: Any] = [
            "uid" : uid as String,
            "timestamp": timestamp as Int64
        ]
        return data
    }
    
}
