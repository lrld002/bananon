//
//  Post.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation

class Post: FirebaseObject {
    
    public static var POST_SUBSCRIBERS = "subscribers"
    
    var uid: String!
    var pid: String!
    var timestamp: Int!
    var text: String!
    var title: String!
    var imgUrl: String!
    var objected: Bool?
    var subscribers: [String]?
    
    init(uid: String, pid: String, text: String, title: String, imgUrl: String, objected: Bool, subscribers: [String]) {
        self.uid = uid
        self.pid = pid
        self.text = text
        self.title = title
        self.imgUrl = imgUrl
        self.objected = objected
        self.subscribers = subscribers
    }
    
    init(dictionary: [String: Any]?) {
        self.uid = dictionary?["uid"] as? String
        self.pid = dictionary?["pid"] as? String
        self.timestamp = dictionary?["timestamp"] as? Int
        self.text = dictionary?["text"] as? String
        self.title = dictionary?["title"] as? String
        self.imgUrl = dictionary?["IMGUrl"] as? String
        self.objected = dictionary?["objected"] as? Bool ?? false
        self.subscribers = dictionary?["subscribers"] as? [String]
    }
    
    func toData() -> [String : Any] {
        let currentTimestamp =  NSDate().timeIntervalSince1970 * 1000
        let data = [
            "uid": uid as Any,
            "pid": pid as Any,
            "timestamp": currentTimestamp.rounded(),
            "text": text as Any,
            "title": title as Any,
            "IMGUrl": imgUrl as Any,
            "objected": objected as Any,
            "subscribers": subscribers as Any
            ] as [String: Any]
        
        return data
    }
    
    static func == (lhs: Post, rhs: Post) -> Bool {
        return lhs.pid == rhs.pid
    }
}

