//
//  TermsViewController.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 21/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation
import UIKit

class TermsViewController: UIViewController {

    private var webView: UIWebView!
    private var toolBar: UIToolbar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
    }
    
    private func setUpViews(){
        title = NSLocalizedString("Terms and conditions of use", comment: "")
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.isNavigationBarHidden = false
        
        webView = {
            let urlView = UIWebView()
            let url = URL(string: "https://sgifer.com/terminos_condiciones.html")
            let req = URLRequest(url: url!)
            urlView.loadRequest(req)
            return urlView
        }()
        
        toolBar = {
            let tb = UIToolbar()
            tb.frame = CGRect(x: 0, y: self.view.frame.size.height - 720, width: self.view.frame.size.width, height: 46)
            tb.sizeToFit()
            tb.backgroundColor = UIColor.white
            tb.isTranslucent = false
            self.view.addSubview(tb)
            return tb
        }()
        
        view.addSubview(webView)
        view.addConstraintWithVisualFormat(format: "V:|-64-[v0]|", views: webView)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: webView)
        
        var items = [UIBarButtonItem]()
        items.append(UIBarButtonItem(title: NSLocalizedString("Terms and conditions of use", comment: ""), style: .plain, target: self, action: nil))
        items.append(UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil))
        items.append(UIBarButtonItem(title: NSLocalizedString("Done", comment: ""), style: .plain, target: self, action: #selector(closeEULA)))
        toolBar.setItems(items, animated: true)
        toolBar.tintColor = #colorLiteral(red: 0, green: 0.7131254077, blue: 0.5890945196, alpha: 1)
        view.addSubview(toolBar)
    }
    
    @objc func closeEULA() {
        dismiss(animated: true, completion: nil)
    }

}
