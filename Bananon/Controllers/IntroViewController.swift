//
//  IntroViewController.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 21/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import UIKit

class IntroViewController: UIViewController {

    @IBOutlet weak var introLogo: UIImageView!
    @IBOutlet weak var introLoginButton: UIButton!
    @IBOutlet weak var eula: UILabel!
    
    @IBOutlet weak var introTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        
        introLogo.image = #imageLiteral(resourceName: "Bananon")
        
        introTitle.text = "Bananon"
        introTitle.font = UIFont.systemFont(ofSize: 36.0)
        introTitle.textAlignment = .center
        introTitle.textColor = UIColor.black
        
        introLoginButton.setTitle(NSLocalizedString("Log in", comment: ""), for: .normal)
        introLoginButton.contentEdgeInsets = UIEdgeInsets(top: 4.0, left: 4.0, bottom: 4.0, right: 8.0)
        introLoginButton.layer.cornerRadius = 15.0
        introLoginButton.clipsToBounds = true
        introLoginButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        introLoginButton.backgroundColor = #colorLiteral(red: 0, green: 0.1764705882, blue: 0.2352941176, alpha: 1)
        
        let normalAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor.init(named: "mainBlueColor")]
        let boldAttributes = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.foregroundColor: UIColor.black]
        let firstText = NSAttributedString(string: NSLocalizedString("By registering, you are accepting the", comment: ""), attributes: normalAttributes as [NSAttributedString.Key : Any])
        let terminosText = NSAttributedString(string: NSLocalizedString("Terms and conditions of use", comment: ""), attributes: boldAttributes)
        let secondText = NSAttributedString(string: NSLocalizedString(" and the ", comment: ""), attributes: normalAttributes as [NSAttributedString.Key : Any])
        let privacidadText = NSAttributedString(string: NSLocalizedString("Privacy policy of Sgifer.", comment: ""), attributes: boldAttributes)
        
        let allText = NSMutableAttributedString(attributedString: firstText)
        allText.append(terminosText)
        allText.append(secondText)
        allText.append(privacidadText)
        
        eula.attributedText = allText
        eula.textAlignment = .center
        eula.numberOfLines = 0
        eula.isUserInteractionEnabled = true
        let eulaGesture = UITapGestureRecognizer(target: self, action: #selector(termsWebPage))
        eula.addGestureRecognizer(eulaGesture)
        
    }
    
    private func setUpConstraints() {
        
        view.addSubview(introLogo)
        let widthConstraint = NSLayoutConstraint(item: introLogo, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 92)
        let centerConstarint = NSLayoutConstraint(item: introLogo, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1.0, constant: 0)
        view.addConstraints([widthConstraint,centerConstarint])
        view.addConstraintWithVisualFormat(format: "V:|-92-[v0(92)]", views: introLogo)
        
        view.addSubview(introTitle)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: introTitle)
        view.addConstraintWithVisualFormat(format: "V:|-240-[v0]", views: introTitle)
        
        view.addSubview(introLoginButton)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: introLoginButton)
        view.addConstraintWithVisualFormat(format: "V:|-480-[v0(32)]", views: introLoginButton)
        
        view.addSubview(eula)
        view.addConstraintWithVisualFormat(format: "H:|-[v0]-|", views: eula)
        view.addConstraintWithVisualFormat(format: "V:[v0]-280-|", views: eula)
    }
    
    @objc func termsWebPage(gestureRecognizer: UIGestureRecognizer) {
        let goTermsActivity = TermsViewController()
        self.present(goTermsActivity, animated: true, completion: nil)
    }
    
}
