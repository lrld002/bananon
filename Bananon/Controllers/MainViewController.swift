//
//  ViewController.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseUI
//import GoogleMobileAds

class MainViewController: UIViewController, UITableViewDelegate, ObjectableDelegate {
    
    @IBOutlet weak var mainTableView: UITableView!
    
    private var dataSource = [AnyObject]()
    
    var selectedPID: String?
    var selectedPTitle: String?
    var selectedPText: String?
    
    //private var adsToLoad = [GADBannerView]()
    private let adInterval = 8
    private let adViewHeigth = CGFloat(320)
    private let adViewWidth = CGFloat(360)
    
    var tableViewDataSource: FUIFirestoreTableViewDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        
        let postButton = UIBarButtonItem(title: NSLocalizedString("Post", comment: ""), style: .done, target: self, action: #selector(publish))
        postButton.tintColor = UIColor.init(named: "mainBlueColor")
        
        self.navigationItem.rightBarButtonItems = [postButton]
        
        mainTableView.delegate = self
        
        let nib = UINib.init(nibName: "commentCell", bundle: nil)
        mainTableView.register(nib, forCellReuseIdentifier: "cell_comment")
        
        tableViewDataSource = mainTableView.bind(toFirestoreQuery: AppDelegate.postsQuery()) { (tableView, indexPath, snapshot) -> UITableViewCell in
            if let cell = tableView.dequeueReusableCell(withIdentifier: "cell_comment", for: indexPath) as? CommentTableViewCell {
                let post = Post(dictionary: self.tableViewDataSource.items[indexPath.row].data())
                
                cell.selectionStyle = .none
                
                cell.commentUserName.font = UIFont.systemFont(ofSize: 36.0)
                
                cell.commentUserText.font = UIFont.systemFont(ofSize: 20.0)
                
                cell.commentBlock.setImage(#imageLiteral(resourceName: "Flag"),for: UIControl.State.normal)
                cell.commentBlock.setTitle("Report post", for: .normal)
                
                cell.objectableDelegate = self
                cell.objected = post.objected ?? false
                cell.PID = post.pid
                
                //cell.postMarkAsObjectable.isHidden = post.uid == AppDelegate.currentUser.uid
                
                cell.addSubview(cell.commentUserName)
                cell.addConstraintWithVisualFormat(format: "H:|-8-[v0]-8-|", views: cell.commentUserName)
                cell.addConstraintWithVisualFormat(format: "V:|-8-[v0]|", views: cell.commentUserName)
                
                cell.addSubview(cell.commentUserText)
                cell.addConstraintWithVisualFormat(format: "H:|-12-[v0]-8-|", views: cell.commentUserText)
                cell.addConstraintWithVisualFormat(format: "V:|-80-[v0]-8-|", views: cell.commentUserText)
                
                cell.addSubview(cell.commentBlock)
                cell.addConstraintWithVisualFormat(format: "H:[v0]-32-|", views: cell.commentBlock)
                cell.addConstraintWithVisualFormat(format: "V:[v0]-32-|", views: cell.commentBlock)
                
                print(post)
                
                cell.commentUserName.text = post.title
                cell.commentUserText.text = post.text
                
                return cell
            } /*else {
                 //It's an add
                 let adView = self.collectionViewDataSource.items[indexPath.row] as! GADBannerView
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AppConstants.AdCellId, for: indexPath)
                 for subview in cell.contentView.subviews {
                 subview.removeFromSuperview()
                 }
                 cell.contentView.addSubview(adView)
                 adView.center = cell.contentView.center
                 
                 self.addBannerAds()
                 self.loadNextAd()
                 
                 return cell
                 }*/
            else {
                return UITableViewCell()
            }
        }
    }
    
    private func setUpConstraints() {
        view.addSubview(mainTableView)
        view.addConstraintWithVisualFormat(format: "H:|-0-[v0]-0-|", views: mainTableView)
        view.addConstraintWithVisualFormat(format: "V:|-0-[v0]-0-|", views: mainTableView)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "show_post_main") {
            let goPost = segue.destination as! PostViewController
            goPost.currentPID = self.selectedPID
            goPost.currentPTitle = self.selectedPTitle
            goPost.currentPText = self.selectedPText
        }
    }
    
    
    @objc func publish() {
        self.performSegue(withIdentifier: "show_publish_main", sender: self)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let post = Post(dictionary: self.tableViewDataSource.items[indexPath.row].data())
        selectedPID = post.pid
        selectedPTitle = post.title
        selectedPText = post.text
        if post.objected == false {
          performSegue(withIdentifier: "show_post_main", sender: self)
        }
        //performSegue(withIdentifier: "show_post_main", sender: self)
    }
    func itemWith(PID: String, didReceive objectableValue: Bool) {
        //tablePosts.child(PID).updateChildValues([Post.POST_OBJECTED: objectableValue])
        let db = Firestore.firestore()
        db.collection(AppConstants.FIREBASE_DATABASE_TABLE_POSTS).document(PID).updateData(["objected" : objectableValue])
    }

    
    /*func checkForPostCountToLoadAd(currentPostNumber: UInt, forTotal: UInt) {
     if currentPostNumber >= forTotal - 1 {
     if let ds = dataSource as? [(user: User, post: Post)] {
     dataSource = ds.sorted(by: { (c1: (user: User, post: Post), c2:(user: User, post: Post)) -> Bool in
     return c1.post.pid.compare(c2.post.pid).rawValue == 1
     }) as [AnyObject]
     }
     self.addBannerAds()
     self.loadNextAd()
     }
     }
     
     private func addBannerAds() {
     var index = 8
     //let size = GADAdSizeFromCGSize(CGSize(width: adViewWidth, height: adViewHeigth))
     let size = GADAdSizeFromCGSize(CGSize(width: 300, height: 250))
     while index < dataSource.count {
     let adView = GADBannerView(adSize: size)
     adView.adUnitID = "ca-app-pub-3574277845682389/9381095689"
     adView.rootViewController = self
     adView.delegate = self
     dataSource.insert(adView, at: index)
     adsToLoad.append(adView)
     index += adInterval
     }
     }
     
     func adViewDidReceiveAd(_ bannerView: GADBannerView) {
     loadNextAd()
     }
     
     func loadNextAd() {
     if !adsToLoad.isEmpty {
     let adView = adsToLoad.removeFirst()
     adView.load(GADRequest())
     }
     }*/
    
    /*func dataAddedWithSnapshot(_ snapshot: DataSnapshot, for type: ObserverType) {
     let postCount = snapshot.childrenCount
     var currentPostNumber: UInt = 0
     let post = Post(dictionary: snapshot.value as! [String: AnyObject])
     
     if (!(dataSource.filter {$0 is (User, Post)} as! [(User, Post)]).contains { (u, p) -> Bool in
     return p.pid == post.pid
     }) {
     print(post)
     //Linea para que se actualicen los posts en tiempo real
     dataSource.insert((AppDelegate.getUser(withId: post.uid), post) as AnyObject, at: 0)
     self.checkForPostCountToLoadAd(currentPostNumber: currentPostNumber, forTotal: postCount)
     currentPostNumber += 1
     //FIXME Provisional
     //postsTableView.reloadData()
     mainCollectionView.reloadData()
     }
     }*/
    
}


