//
//  PostViewController.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation
import UIKit
import FirebaseUI
//import OneSignal

class PostViewController: UIViewController, UITableViewDelegate {
    
    @IBOutlet weak var postTitle: UILabel!
    @IBOutlet weak var postText: UITextView!
    @IBOutlet weak var postCommentsTableView: UITableView!
    @IBOutlet weak var postCommentText: UITextField!
    @IBOutlet weak var postCommentButton: UIButton!
    
    private var post: Post!
    
    private var currentUser: User!
    private var postOwner: User!
    
    var currentPID: String!
    var currentPTitle: String!
    var currentPText: String!
    
    let db = Firestore.firestore()
    
    var tableViewDataSource: FUIFirestoreTableViewDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        setUpConstraints()
        hideKeyboardWhenTappedArround()
        
        postCommentsTableView.delegate = self
        
        let nib = UINib.init(nibName: "commentCell", bundle: nil)
        postCommentsTableView.register(nib, forCellReuseIdentifier: "cell_comment")
        
        tableViewDataSource = postCommentsTableView.bind(toFirestoreQuery: AppDelegate.commentsQuery()) { (tableView, indexPath, snapshot) -> UITableViewCell in
            // Dequeue cell
            if let cell = tableView.dequeueReusableCell(withIdentifier: "cell_comment", for: indexPath) as? CommentTableViewCell {
                let comment = Comment(dictionary: self.tableViewDataSource.items[indexPath.row].data())
                print(comment)
                
                cell.commentUserText.text = comment.text
                cell.commentBlock.isHidden = true
                
                if var timeResult = comment.timestamp{
                    timeResult = timeResult / 1000
                    let date = NSDate(timeIntervalSince1970: TimeInterval(timeResult))
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "EEE dd/MM/yyyy hh:mm a"
                    let localDate = dateFormatter.string(from: date as Date)
                    cell.commentUserName.text = localDate
                }
                
                cell.addSubview(cell.commentUserName)
                cell.addConstraintWithVisualFormat(format: "H:|-8-[v0]-8-|", views: cell.commentUserName)
                cell.addConstraintWithVisualFormat(format: "V:|-8-[v0]|", views: cell.commentUserName)
                
                cell.addSubview(cell.commentUserText)
                cell.addConstraintWithVisualFormat(format: "H:|-8-[v0]-8-|", views: cell.commentUserText)
                cell.addConstraintWithVisualFormat(format: "V:|-64-[v0]-8-|", views: cell.commentUserText)
                
                return cell
            }
            else {
                return UITableViewCell()
            }
        }
        
    }
    
    private func setUpViews() {
        postTitle.text = currentPTitle
        postTitle.textAlignment = .center
        postTitle.font = UIFont.systemFont(ofSize: 36.0)
        
        postText.text = currentPText
        postText.textAlignment = .left
        postText.font = UIFont.systemFont(ofSize: 20.0)
        
        postCommentText.placeholder = NSLocalizedString("Write something", comment: "")
        
        let postGesture = UITapGestureRecognizer(target: self, action: #selector (doComment(sender:)))
        postCommentButton.addGestureRecognizer(postGesture)
        postCommentButton.setImage(#imageLiteral(resourceName: "Send"), for: .normal)
        postCommentButton.setTitle(nil, for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIApplication.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(adjustForKeyboard), name: UIApplication.keyboardWillChangeFrameNotification, object: nil)
        
    }
    
    private func setUpConstraints() {
        
        view.addSubview(postTitle)
        view.addConstraintWithVisualFormat(format: "H:|-8-[v0]-8-|", views: postTitle)
        view.addConstraintWithVisualFormat(format: "V:|-80-[v0(80)]", views: postTitle)
        
        view.addSubview(postText)
        view.addConstraintWithVisualFormat(format: "H:|-8-[v0]-8-|", views: postText)
        view.addConstraintWithVisualFormat(format: "V:|-160-[v0]-340-|", views: postText)
        
        view.addSubview(postCommentsTableView)
        view.addConstraintWithVisualFormat(format: "V:|-400-[v0]-48-|", views: postCommentsTableView)
        view.addConstraintWithVisualFormat(format: "H:|[v0]|", views: postCommentsTableView)
        
        view.addSubview(postCommentButton)
        view.addConstraintWithVisualFormat(format: "V:[v0]-10-|", views: postCommentButton)
        view.addConstraintWithVisualFormat(format: "H:[v0]-16-|", views: postCommentButton)
        
        view.addSubview(postCommentText)
        view.addConstraintWithVisualFormat(format: "V:[v0]-8-|", views: postCommentText)
        view.addConstraintWithVisualFormat(format: "H:|-8-[v0]-56-|", views: postCommentText)
        
    }
    
    @objc private func doComment(sender: UIButton!){
        let currentTimestamp = NSTimeIntervalSince1970 * 1000
        let CID = self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_COMMENTS).document().documentID
        
        if let commentText = postCommentText.text {
            
            if !commentText.isEmpty {
                
                let comment = Comment(uid: AppDelegate.currentUser.uid, cid: CID, pid: currentPID, timestamp: Int(currentTimestamp), text: postCommentText.text!)
                self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_COMMENTS).document(CID).setData(comment.toData())
                self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_COMMENTS).document(CID).setData(["cid": String(CID),], merge: true)
                
                if post != nil {
                    if post.subscribers == nil {
                        post.subscribers = [String]()
                    }
                    if !(post.subscribers?.contains(AppDelegate.currentUser.uid))! {
                        post.subscribers?.append(AppDelegate.currentUser.uid)
                    }
                }
                
                if post != nil
                    && post.subscribers != nil {
                    self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_POSTS).document(currentPID).setData([Post.POST_SUBSCRIBERS: post.subscribers!])
                    
                    let additionalData: [String: Any] = [
                        AppConstants.ONESIGNAL_KEY_NOTIFICATION_TYPE: NotificationType.UserInteraction.rawValue,
                        AppConstants.ONESIGNAL_KEY_POST_ID: currentPID as Any,
                        AppConstants.ONESIGNAL_KEY_NOTIFICATION_CONTENT: comment.text as Any
                    ]
                    sendComment(postOwner, from: currentUser, to: post.subscribers ?? [], with: additionalData)
                    //sendCommentNotificationToPostOwnedBy(postOwner, from: currentUser, to: post.subscribers!, with: pushId!, andWith: additionalData)
                }
            }
        }
        view.endEditing(true)
        if postCommentsTableView.isHidden == true {
            postCommentsTableView.isHidden = false
        }
        if postCommentText.isEditing == false {
            postCommentText.text = ""
        }
        postCommentText.clearsOnBeginEditing = true
    }
    
    private func sendComment(_ owner: User, from: User, to: [String], with additionalData: [String: Any]) {
        let uids = to.filter { (uid) -> Bool in
            return uid != from.uid
        }
        for uid in uids {
            
            self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_PUSH_IDS).document(uid).addSnapshotListener { (snapshot, error) in
                if error == nil {
                    //let subscriptor = PostSubscriptor(dictionary: snapshot?.value as [String : Any])
                    let subscriptor = PostSubscriptor(dictionary: snapshot?.data())
                    self.sendCommentNotificationToPostOwnedBy(owner, from: from, to: subscriptor.pushIds ?? [], with: additionalData)
                }
            }
            
            /*tablePushIds.child(uid).observeSingleEvent(of: .value, with: { snapshot in
             if snapshot.exists() {
             let subscriptor = SgiferPostSubscriptor(snapshot: snapshot.value as! [String: AnyObject])
             self.sendCommentNotificationToPostOwnedBy(owner, from: from, to: subscriptor.pushIds ?? [], with: additionalData)
             }
             })*/
        }
    }
    
    private func sendCommentNotificationToPostOwnedBy(_ owner: User, from: User, to: [String], with additionalData: [String: Any]?) {
        let contents: [String: Any] = [
            "en": ("Someone has leaved a comment on a post"),
            "es": ( "Alguien dejó un comentario en un post")
        ]
        let data = NSMutableDictionary()
        data.setValue(from.uid, forKey: AppConstants.ONESIGNAL_KEY_SENDER_NAME)
        /*data.setValue((from.photoUrl.isEmpty ? AppConstants.SGIFER_DEFAULT_PROFILE_IMAGE_URL : from.photoUrl), forKey: AppConstants.ONESIGNAL_KEY_SENDER_PHOTO_URL)
         data.setValue(to, forKey: AppConstants.ONESIGNAL_KEY_SENT_TO)
         additionalData?.forEach({ (key: String, value: Any) in
         data.setValue(value, forKey: key)
         })*/
        
        let notification: [String: Any] = [
            AppConstants.ONESIGNAL_KEY_CONTENTS: contents,
            AppConstants.ONESIGNAL_KEY_INCLUDE_PLAYER_IDS: to,
            AppConstants.ONESIGNAL_KEY_DATA: data
        ]
        
        //OneSignal.postNotification(notification)
    }
    
}

