//
//  PublishViewController.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseStorage
import Photos

class PublishViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var publishTitle: UITextField!
    @IBOutlet weak var publishText: UITextField!
    @IBOutlet weak var publishButton: UIButton!
    @IBOutlet weak var publishImage: UIImageView!
    
    private var imageDataSource: Int!
    private var uploadTask: StorageUploadTask!
    private var uploadPercent = 0.0
    
    var imageURL: URL!
    
    let db = Firestore.firestore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpViews()
        setUpConstraints()
    }
    
    private func setUpViews() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: #selector (dismissPicker))
        let spacer = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let sendButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Send"), style: .done, target: self, action: #selector(doPost))
        toolbar.setItems([cancelButton, spacer,sendButton], animated: false)
        
        publishTitle.placeholder = NSLocalizedString("Write a phrase", comment: "")
        publishTitle.inputAccessoryView = toolbar
        
        publishText.placeholder = NSLocalizedString("Write something", comment: "")
        publishText.inputAccessoryView = toolbar
        
        let publishGesture = UITapGestureRecognizer(target: self, action: #selector (doPost))
        publishButton.addGestureRecognizer(publishGesture)
        publishButton.setImage(#imageLiteral(resourceName: "Send"), for: .normal)
        publishButton.isHidden = true
        
    }
    
    private func setUpConstraints() {
        view.addSubview(publishTitle)
        view.addConstraintWithVisualFormat(format: "H:|-8-[v0]-8-|", views: publishTitle)
        view.addConstraintWithVisualFormat(format: "V:|-80-[v0(80)]", views: publishTitle)
        
        view.addSubview(publishText)
        view.addConstraintWithVisualFormat(format: "H:|-8-[v0]-8-|", views: publishText)
        view.addConstraintWithVisualFormat(format: "V:|-160-[v0]-8-|", views: publishText)
    }
    
    @objc func doPost() {
        var subscriptors = [String]()
        subscriptors.append((AppDelegate.currentUser.uid)!)
        
        let PID = self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_POSTS).document().documentID
        let post = Post(uid: (AppDelegate.currentUser.uid)!, pid: PID, text: self.publishText.text!, title: self.publishTitle.text!, imgUrl: "downloadUrl.absoluteString", objected: false, subscribers: subscriptors)
        self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_POSTS).document(PID).setData(post.toData())
        self.db.collection(AppConstants.FIREBASE_DATABASE_TABLE_POSTS).document(PID).setData(["pid": String(PID),], merge: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dismissPicker() {
        view.endEditing(true)
    }
    
}

