//
//  CommentTableViewCell.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation
import UIKit

class CommentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var commentUserName: UILabel!
    @IBOutlet weak var commentUserText: UILabel!
    @IBOutlet weak var commentBlock: UIButton!
    
    var postMarkedAsObjectableMessage: UILabel!
    var postMarkAsObjectable: UIImageView!

    var PID: String!

    var objectableDelegate: ObjectableDelegate!
    
    var objected: Bool = false {
        didSet{
            if objected != oldValue {
                if objected {
                    //postMarkAsObjectable.image = UIImage(named: "Reportado")
                    commentUserText.isHidden = true
                    commentUserName.isHidden = true
                    //postMarkedAsObjectableMessage.text = reportedMessage
                } else {
                    //postMarkAsObjectable.image = UIImage(named: "Reportar")
                    commentUserText.isHidden = false
                    commentUserName.isHidden = false
                    //postMarkedAsObjectableMessage.text = ""
                }
                objectableDelegate.itemWith(PID: PID, didReceive: objected)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        //setUpViews()
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        commentBlock.addGestureRecognizer(gestureRecognizer)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    private func setUpViews() {
        
        postMarkedAsObjectableMessage = {
            let msg = UILabel()
            msg.font = UIFont.systemFont(ofSize: 11)
            msg.textAlignment = .right
            return msg
        }()
        
        postMarkAsObjectable = {
            let mao = UIImageView()
            mao.isUserInteractionEnabled = true
            mao.image = UIImage(named: "Reportar")
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
            mao.addGestureRecognizer(gestureRecognizer)
            return mao
        }()
        
        //contentView.addSubview(postMarkedAsObjectableMessage)
        //contentView.addSubview(postMarkAsObjectable)
        //contentView.addConstraintWithVisualFormat(format: "H:|-[v0]-[v1(24)]-|", views: postMarkedAsObjectableMessage, postMarkAsObjectable)
        
    }
    
    @objc func handleTap(gestureRecognizer: UIGestureRecognizer){
        objected = !objected
    }

    
}

