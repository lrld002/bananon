//
//  Appconstants.swift
//  Bananon
//
//  Created by Gerardo García Velázquez on 10/07/19.
//  Copyright © 2019 Gerardo García Velázquez. All rights reserved.
//

import Foundation
import FirebaseDatabase

class AppConstants: NSObject {
    
    public static let FIREBASE_DATABASE_TABLE_ITEMS = "items"
    public static let FIREBASE_DATABASE_TABLE_USERS = "users"
    public static let FIREBASE_DATABASE_TABLE_CARTS = "carts"
    public static let FIREBASE_DATABASE_TABLE_CARDS = "cards"
    public static let FIREBASE_DATABASE_TABLE_ORDERS = "orders"
    public static let FIREBASE_DATABASE_TABLE_POSTS = "posts"
    public static let FIREBASE_DATABASE_TABLE_COMMENTS = "comments"
    public static let FIREBASE_DATABASE_TABLE_PUSH_IDS = "pushids"
    
    static func addPushId(_ pushId: String , toUserWithId uid: String) {
        let pushIdsTable = Database.database().reference(withPath: FIREBASE_DATABASE_TABLE_PUSH_IDS)
        
        pushIdsTable.child(uid).observeSingleEvent(of: .value, with: { snapshot in
            let subscriptor = PostSubscriptor(dictionary: snapshot.value as! [String: AnyObject])
            
            if subscriptor.pushIds != nil {
                if !(subscriptor.pushIds?.contains(pushId))! {
                    subscriptor.pushIds?.append(pushId)
                }
            } else {
                subscriptor.pushIds = [String]()
                subscriptor.pushIds?.append(pushId)
            }
            pushIdsTable.child(uid).setValue(subscriptor.toData())
        })
    }
    
    public static let AdCellId = "ACID"
    
    public static let ONESIGNAL_KEY_CONTENTS = "contents"
    public static let ONESIGNAL_KEY_INCLUDE_PLAYER_IDS = "include_player_ids"
    public static let ONESIGNAL_KEY_DATA = "data"
    public static let ONESIGNAL_KEY_SENT_TO = "sentTo"
    public static let ONESIGNAL_KEY_NOTIFICATION_TYPE = "type"
    public static let ONESIGNAL_KEY_POST_ID = "pid"
    public static let ONESIGNAL_KEY_NOTIFICATION_CONTENT = "content"
    public static let ONESIGNAL_KEY_SENDER_PHOTO_URL = "phurl"
    public static let ONESIGNAL_KEY_SENDER_NAME = "sendername"
    public static let ONESIGNAL_KEY_TARGET_PLATFORM = "platform"
    
    public static let ONESIGNAL_KEY_ANNOUNCEMENT_TITLE = "announce_title"
    public static let ONESIGNAL_KEY_ANNOUNCEMENT_CONTENT = "announce_content"
    public static let ONESIGNAL_KEY_ANNOUNCEMENT_ANDROID_URL = "announce_android_url"
    public static let ONESIGNAL_KEY_ANNOUNCEMENT_IOS_URL = "announce_ios_url"
    
    //public static var FIREBASE_STORAGE_BUCKET_URL = "gs://bananon-1d846.appspot.com/"
}

